/*
2. Carrera de animales. Se crearán 3 hilos que representarán a tres animales, un Guepardo,
una libre y una tortuga. ¿Quién ganará la carrera? Los hilos deberán de hacer una cuenta
atrás de 30 segundos escribiendo mensajes. Al finalizar la carrera se deberá indicar el
orden de los participantes
 */

import java.util.ArrayList;

public class Ejercicio2 {
    public static void main(String[] args) {
        Thread2_1 hilo1 = new Thread2_1();
        Thread2_2 hilo2 = new Thread2_2();
        Thread2_3 hilo3 = new Thread2_3();
        ArrayList<String> orden = new ArrayList<>();

        boolean h1 = true;
        boolean h2 = true;
        boolean h3 = true;

        hilo1.start(); //guepardo
        hilo2.start(); //libre
        hilo3.start(); //tortuga

        while (hilo1.isAlive() || hilo2.isAlive() || hilo3.isAlive()){
            if (!hilo1.isAlive() && h1){
                h1 = false;
                orden.add("Guepardo");
            }
            else if(!hilo2.isAlive() && h2){
                h2 = false;
                orden.add("Liebre");
            }
            else if (!hilo3.isAlive() && h3){
                h3 = false;
                orden.add("Tortuga");
            }
        }

        if(h1){
            orden.add("Guepardo");
        }
        else if (h2){
            orden.add("Liebre");
        }
        else if (h3){
            orden.add("Tortuga");
        }

        for (int i = 0; i < orden.size();i++){
            System.out.println((i+1) + " - " + orden.get(i));
        }

    }
}
