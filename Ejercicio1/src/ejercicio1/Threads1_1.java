package ejercicio1;

import java.util.stream.IntStream;

public class Threads1_1 extends Thread{
    int vectorEntero[];

    public Threads1_1(int[] vectorEntero) {
        this.vectorEntero = vectorEntero;
    }

    public void run(){
        suma();
    }

    public void suma(){
        System.out.println(IntStream.of(vectorEntero).sum());
    }




}
