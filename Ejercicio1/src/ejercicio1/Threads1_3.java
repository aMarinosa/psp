package ejercicio1;

import java.util.stream.IntStream;

public class Threads1_3 extends Thread{
    int[] vectorEntero;
    public Threads1_3(int[] vectorEntero) {
        this.vectorEntero = vectorEntero;
    }

    public void run(){
        media();
    }

    public void media(){

        int suma = IntStream.of(vectorEntero).sum();
        int media = suma/6;
        System.out.println(media);
    }
}
