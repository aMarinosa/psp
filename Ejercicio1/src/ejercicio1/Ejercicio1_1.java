package ejercicio1;

import java.util.Arrays;

public class Ejercicio1_1 {
    public static void main(String[] args) {
        int vectorEntero[] = {1, 2, 3, 4, 5};
        Thread hilo1 = new Threads1_1(vectorEntero);
        Thread hilo2 = new Threads1_2(vectorEntero);
        Thread hilo3 = new Threads1_3(vectorEntero);

        System.out.println("Vector utilizado: "+Arrays.toString(vectorEntero));
        hilo1.start();
        hilo2.start();
        hilo3.start();



    }
}
