package ejercicio1;

public class Threads1_2 extends Thread{
    int vectorEntero[];

    public Threads1_2(int[] vectorEntero) {
        this.vectorEntero = vectorEntero;
    }
    public void run(){
        sumaCuadrados();
    }

    public void sumaCuadrados(){
        int suma= 0;
        for (int i = 0; i < vectorEntero.length ; i++){
            suma += Math.pow(vectorEntero[i], 2);
        }
        System.out.println(suma);
    }
}
