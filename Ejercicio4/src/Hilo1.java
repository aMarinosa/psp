import java.util.concurrent.Semaphore;

public class Hilo1 extends Thread {

    Semaphore s1;
    Semaphore s2;

    public Hilo1(Semaphore s1, Semaphore s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++){
            try {
                s1.acquire();
                si();
                s2.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void si(){
        System.out.print("SI ");
    }
}
