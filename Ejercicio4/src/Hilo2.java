import java.util.concurrent.Semaphore;

public class Hilo2 extends Thread {

    Semaphore s1;
    Semaphore s2;

    public Hilo2(Semaphore s1, Semaphore s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++){
            try {
                s2.acquire();
                no();
                s1.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void no(){
        System.out.print("NO ");
    }
}
