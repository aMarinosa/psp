import java.util.concurrent.Semaphore;

public class Main {
    /*
        Escribe una aplicación que tenga dos hilos. Un hilo escribirá “SI” mientas que el otro
        escribirá “NO”. Cada hilo escribirá 10 veces su mensaje. La salida deberá de ser la
        siguiente:
                 SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO SI NO
     */
    public static void main(String[] args) {

        Semaphore s1 = new Semaphore(1);
        Semaphore s2 = new Semaphore(0);

        Hilo1 h1 = new Hilo1(s1, s2);
        Hilo2 h2 = new Hilo2(s1, s2);

        h1.start();
        h2.start();

    }
}
