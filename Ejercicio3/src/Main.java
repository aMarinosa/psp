public class Main {
    public static void main(String[] args) {
        Hilo1 h1 = new Hilo1();
        Hilo2 h2 = new Hilo2();

        h1.start();
        try {
            h1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        h2.start();
    }
}
